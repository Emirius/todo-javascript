function addToList() {
  let taskName = document.getElementById('taskname').value;
  let todoDiv = document.getElementById('mytodo');

  let newTodoItem = document.createElement('div');
  let todoName = document.createElement('li');
  todoName.innerHTML = taskName;

  // Delete Button
  let deleteBtn = document.createElement('i');
  deleteBtn.classList.add('far');
  deleteBtn.classList.add('fa-trash-alt');

  newTodoItem.appendChild(todoName);
  newTodoItem.appendChild(deleteBtn);

  todoDiv.appendChild(newTodoItem);
}

let todoDiv = document.getElementById('mytodo');
todoDiv.addEventListener('click', deleteItem);

function deleteItem(e) {
  const element = e.target;
  if (element.classList[0] === 'far') {
    element.parentElement.remove();
  }
}
